<div align="center">

#### Seja muito bem-vindo a [botecorpm.gitlab.io](https://gitlab.com/botecorpm/botecorpm.gitlab.io)
Para ajudar no projeto por favor ver [botecorpmpackagers.gitlab.io](https://gitlab.com/botecorpm/repo/botecorpmrepo.gitlab.io) ou acessar o canal de telegram [Comunidade Mageia do Brasil](https://t.me/comunidademageiaBR).

<p align="center">
  <img src="logo.png" width="230" />
</p>
</div>

#### Introdução
BotecoRPM é um projeto social educativo, pretende coletar, unificar, aprimorar, facilitar e disponibilizar de maneira intuitiva recursos sobre empacotamento RPM, a fim de ser uma ferramenta de aprendizado fácil acesso e compreensão.
Também de servir como repositório de pacotes para diversas distros que usam RPM, e principalmente apoiando futuros profissionais de Tecnologia da Informação possibilitando a troca de experiência e ajuda. 



#### Listado de pacotes RPM
[Mageia](/repo/mageia)
- [ ] [Nome](Site)
- [ ] [Nome](Site)

[OpenSUSE](/repo/opensuse)
- [ ] [Nome](Site)
- [ ] [Nome](Site)

[PCLinuxOS](/repo/pclinuxos)
- [ ] [Nome](Site)
- [ ] [Nome](Site)

[Fedora](/repo/fedora)
- [ ] [Nome](Site)
- [ ] [Nome](Site)
